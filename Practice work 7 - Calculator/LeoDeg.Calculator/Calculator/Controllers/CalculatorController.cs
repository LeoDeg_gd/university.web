﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Calculator.Models;
using Microsoft.AspNetCore.Mvc;

namespace Calculator.Controllers
{
	public class CalculatorController : Controller
	{
		private readonly CalculatorMachine calculator;

		public CalculatorController(CalculatorMachine calculator)
		{
			this.calculator = calculator;
		}

		public IActionResult Index(CalculationInfoDTO information)
		{
			return View(information ?? new CalculationInfoDTO());
		}

		public IActionResult Calculate(CalculationInfoDTO information)
		{
			if (!ModelState.IsValid)
			{
				return RedirectToAction(nameof(Index), information);
			}

			try
			{
				information.Result = calculator.Calculate(
					information.X,
					information.Y,
					information.Operation
				);
			}
			catch (DivideByZeroException)
			{
				information.Result = 0;
				information.ErrorMessage = "Вы не можете делить на нуль!";
			}

			return RedirectToAction(nameof(Index), information);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Calculator.Models
{
	public class CalculatorMachine
	{
		public enum Operation
		{
			[Display(Name = "+")]
			Add,
			[Display(Name = "-")]
			Substract,
			[Display(Name = "*")]
			Multiply,
			[Display(Name = "/")]
			Divide,
			[Display(Name = "mod")]
			Mod
		}

		public Dictionary<Operation, Func<int, int, int>> Expressions = new Dictionary<Operation, Func<int, int, int>>
		{
			{ Operation.Add, (x, y) => x + y },
			{ Operation.Substract, (x, y) => x - y },
			{ Operation.Multiply, (x, y) => x * y },
			{ Operation.Divide, (x, y) => x / y },
			{ Operation.Mod, (x, y) => x % y },
		};

		public int Calculate(int x, int y, Operation operation)
		{
			Func<int, int, int> expression;
			Expressions.TryGetValue(operation, out expression);
			return expression.Invoke(x, y);
		}
	}
}

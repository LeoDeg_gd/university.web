﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Calculator.Models
{
	public class CalculationInfoDTO
	{
		[Required]
		[Display(Name = "Первое значение")]
		public int X { get; set; }

		[Required]
		[Display(Name = "Второе значение")]
		public int Y { get; set; }

		[Required]
		[Display(Name = "Операция")]
		public CalculatorMachine.Operation Operation { get; set; }

		public int Result { get; set; }
		public string ErrorMessage { get; set; }
	}
}

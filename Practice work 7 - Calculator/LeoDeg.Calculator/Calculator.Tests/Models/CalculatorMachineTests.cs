﻿using NUnit.Framework;
using Calculator.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator.Models.Tests
{
	[TestFixture()]
	public class CalculatorMachineTests
	{
		private CalculatorMachine calculator;

		[SetUp]
		public void Setup ()
		{
			calculator = new CalculatorMachine();
		}

		[Test()]
		[TestCase(10,20, CalculatorMachine.Operation.Add, 30)]
		[TestCase(10,20, CalculatorMachine.Operation.Substract, -10)]
		[TestCase(20,10, CalculatorMachine.Operation.Divide, 2)]
		[TestCase(5,10, CalculatorMachine.Operation.Multiply, 50)]
		[TestCase(9,4, CalculatorMachine.Operation.Mod, 1)]
		public void CalculateTest(int x, int y, CalculatorMachine.Operation operation, int expected)
		{
			Assert.AreEqual(expected, calculator.Calculate(x, y, operation));
		}
	}
}
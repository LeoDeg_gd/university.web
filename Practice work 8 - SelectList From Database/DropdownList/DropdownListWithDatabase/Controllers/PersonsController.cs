﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DropdownListWithDatabase.Data;
using DropdownListWithDatabase.Models;
using Microsoft.AspNetCore.Mvc;

namespace DropdownListWithDatabase.Controllers
{
	public class PersonsController : Controller
	{
		private readonly IPersonRepository personRepository;

		private const string PersonForm = "PersonForm";

		public PersonsController(IPersonRepository personRepository)
		{
			this.personRepository = personRepository;
		}

		public IActionResult Index()
		{
			return View(personRepository.Get());
		}

		public IActionResult Create()
		{
			return View(PersonForm, new Person());
		}

		public IActionResult Edit(int id)
		{
			return View(PersonForm, personRepository.Get(id));
		}

		public IActionResult Details(int id)
		{
			return View(personRepository.Get(id));
		}

		public IActionResult Save(Person person)
		{
			if (!ModelState.IsValid)
				return View(person);

			if (person.Id == 0)
				personRepository.Add(person);
			else personRepository.Update(person.Id, person);
			personRepository.SaveChanges();

			return RedirectToAction(nameof(Index));
		}

		public IActionResult Delete(int id)
		{
			personRepository.Delete(id);
			personRepository.SaveChanges();

			return RedirectToAction(nameof(Index));
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DropdownListWithDatabase.Data;
using Microsoft.AspNetCore.Mvc;

namespace DropdownListWithDatabase.Controllers
{
    public class DropdownListController : Controller
    {
        private readonly IPersonRepository personRepository;

        public DropdownListController(IPersonRepository personRepository)
        {
            this.personRepository = personRepository;
        }

        public IActionResult Index()
        {
            return View(personRepository.Get());
        }

        public IActionResult Result (int id)
        {
            DropdownResultVM vm = new DropdownResultVM();

            var person = personRepository.Get(id);
            if (person != null)
            {
                vm.Person = person;
                vm.Message = "Успех. Данные успешно отправлены.";
            }
            else
            {
                vm.Message = "Данные не были отправлены. Произошла ошибка. Пользователь не был выбран.";
            }

            return View(vm);
        }
    }
}
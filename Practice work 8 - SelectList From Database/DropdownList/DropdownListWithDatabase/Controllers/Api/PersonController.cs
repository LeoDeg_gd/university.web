﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using DropdownListWithDatabase.Data;
using DropdownListWithDatabase.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DropdownListWithDatabase.Controllers.Api
{
    [ApiController]
    [Route("api/[controller]")]
    public class PersonController : ControllerBase
    {
        private readonly IPersonRepository personRepository;

        public PersonController(IPersonRepository personRepository)
        {
            this.personRepository = personRepository;
        }

        [HttpGet()]
        public IActionResult GetByPassportSerialNumber (string serialNumber)
        {
            var persons = personRepository.GetPersonsByPassportSerialNumber(serialNumber);
            return new ObjectResult(persons);
        }
    }
}
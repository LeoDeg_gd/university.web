﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DropdownListWithDatabase.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;

namespace DropdownListWithDatabase.Data
{
	public interface IRepository<T> where T : class
	{
		T Get(int id);
		IEnumerable<T> Get();
		void Add(T entity);
		void Update(int id, T newEntity);
		bool Delete(int id);
		bool SaveChanges();
	}
}

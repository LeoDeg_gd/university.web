﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DropdownListWithDatabase.Models;
using Microsoft.EntityFrameworkCore;

namespace DropdownListWithDatabase.Data
{
	public class ApplicationDbContext : DbContext
	{
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base (options)
		{

		}

		public DbSet<Person> Persons { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DropdownListWithDatabase.Models;

namespace DropdownListWithDatabase.Data
{
	public class DropdownResultVM
	{
		public Person Person { get; set; }
		public string Message { get; set; }
	}
}

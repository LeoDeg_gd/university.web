﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DropdownListWithDatabase.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;

namespace DropdownListWithDatabase.Data
{
	public class PersonRepository : IPersonRepository
	{
		private readonly ApplicationDbContext context;

		public PersonRepository(ApplicationDbContext context)
		{
			this.context = context;
		}

		public void Add(Person entity)
		{
			context.Persons.Add(entity);
		}

		public Person Get(int id)
		{
			return context.Persons.AsNoTracking().SingleOrDefault(x => x.Id == id);
		}

		public IEnumerable<Person> Get()
		{
			return context.Persons.AsNoTracking().ToList();
		}

		public bool Delete(int id)
		{
			var person = context.Persons.SingleOrDefault(x => x.Id == id);
			if (person != null)
			{
				context.Persons.Remove(person);
				return true;
			}

			return false;
		}

		public void Update(int id, Person newEntity)
		{
			if (newEntity == null)
				throw new ArgumentNullException();

			var person = context.Persons.SingleOrDefault(x => x.Id == id);
			if (person == null)
				throw new InvalidOperationException($"Person with id [{id}] not found!");

			person.FirstName = newEntity.FirstName;
			person.LastName = newEntity.LastName;
			person.MiddleName = newEntity.MiddleName;
			person.PassportSeries = newEntity.PassportSeries;
			person.PassportSerialNumber = newEntity.PassportSerialNumber;
		}

		public bool SaveChanges()
		{
			return context.SaveChanges() > 0;
		}

		public Person GetByPassportSerialNumber(string serialNumber)
		{
			return context.Persons.AsNoTracking().SingleOrDefault(x => x.PassportSerialNumber == serialNumber);
		}

		public IEnumerable<Person> GetPersonsByPassportSerialNumber(string serialNumber)
		{
			return context.Persons.AsNoTracking().Where(x => x.PassportSerialNumber.Contains(serialNumber));
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DropdownListWithDatabase.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;

namespace DropdownListWithDatabase.Data
{
	public interface IPersonRepository : IRepository<Person>
	{
		Person GetByPassportSerialNumber(string serialNumber);
		IEnumerable<Person> GetPersonsByPassportSerialNumber(string serialNumber);
	}
}

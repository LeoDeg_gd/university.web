﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DropdownListWithDatabase.Models
{
	public class Person
	{
		public int Id { get; set; }
		[Required]
		[Display(Name = "Имя")]
		public string FirstName { get; set; }
		[Required]
		[Display(Name = "Фамилия")]
		public string LastName { get; set; }
		[Display(Name = "Отчество")]
		public string MiddleName { get; set; }
		[Required]
		[Display(Name = "Серия паспорта")]
		public string PassportSeries { get; set; }
		[Required]
		[Display(Name = "Номер паспорта")]
		public string PassportSerialNumber { get; set; }
	}
}

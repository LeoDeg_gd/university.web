const emailTest = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const onlyLettersTest = /^[a-zA-Z]*$/g;
const phoneTest = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;

// Цены товаров, заменяют информацию из базы данных
let priceMap = new Map([
	["1", 500],
	["2", 50],
	["3", 200],
	["4", 150]
]);

function calculateTotalPrice(amount) {
	var selected = document.getElementById("items");
	var selectedValue = selected.options[selected.selectedIndex].value;

	if (selectedValue > 0) {
		var price = priceMap.get(selectedValue) * parseInt(amount.value);
		var text = document.createTextNode("Сумма заказа: " + price + " рублей");

		document.getElementById("totalPrice").innerText = "";
		document.getElementById("totalPrice").appendChild(text);
	}
}

function normal(object) {
	object.style.borderColor = "#ccc";
}

function danger(object) {
	object.style.borderColor = "red";
}

function resetBorderColors() {
	normal(document.getElementById("items"));
	document.querySelectorAll("input").forEach(item => {
		normal(item);
	});
}

function checkValidation(object) {
	normal(object);
	if (object.value == 0 || object.value == "") {
		danger(object);
	}
}

function checkPhoneValidation(phone) {
	normal(phone);
	if (phoneTest.test(phone.value) == false) {
		danger(phone);
	}
}

function checkEmailValidation(email) {
	normal(email);
	if (emailTest.test(email.value) == false) {
		danger(email);
	}
}

function checkOnlyLetters(letters) {
	normal(letters);
	if (!onlyLettersTest.test(letters.value)) {
		danger(letters);
	}
}

function checkAllValidation() {
	checkSelectedItems();
	checkTextInputs();
	checkIsNumber();
}

function checkIsNumber() {
	normal(document.getElementById("amount"));
	if (!parseInt(document.getElementById("amount").value)) {
		danger(document.getElementById("amount"));
	}
}

function checkSelectedItems() {
	normal(document.getElementById("items"));
	if (document.getElementById("items").value == 0) {
		danger(document.getElementById("items"));
	}
}

function checkTextInputs() {
	document.querySelectorAll("input").forEach(item => {
		normal(item);
		if (item.required && (item.value == 0 || item.value == "")) {
			danger(item);
		}
	});
}
